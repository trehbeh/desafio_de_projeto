const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let heroName = "JavaScript"

rl.question("Digite o nível em que o usuário está?: ", function(xp) {
    xp = parseInt(xp)
    let level = " "
    
    if (xp < 0) {
        console.log("Valor inválido!")
        rl.close();
        return;
    } else if (xp <= 1000) {
        level = "Ferro" //Se XP for menor do que 1.000 = Ferro
    } else if (xp <= 2000) {
        level = "Bronze" //Se XP for entre 1.001 e 2.000 = Bronze
    } else if (xp <= 5000) {
        level = "Prata" //Se XP for entre 2.001 e 5.000 = Prata
    } else if (xp <= 7000) {
        level = "Ouro" //Se XP for entre 5.001 e 7.000 = Ouro
    } else if (xp <= 8000) {
        level = "Platina" //Se XP for entre 7.001 e 8.000 = Platina"
    } else if (xp <= 9000) {
        level = "Ascendente"; //Se XP for entre 8.001 e 9.000 = Ascendente"
    } else if (xp <= 10000) {
        level = "Imortal" //Se XP for entre 9.001 e 10.000= Imortal
    } else {
        level = "Radiante" //Se XP for maior ou igual a 10.001 = Radiante
    }

    console.log("O Herói de nome" + heroName + "está no nível de " + level)

    rl.close();
})